package se.tablemobilegames.klunkwars;

import android.os.Bundle;
import se.tablemobilegames.klunkwars.application.KlunkwarsGame;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
	      config.useAccelerometer = false;
	      config.useCompass = false;
		initialize(new KlunkwarsGame(), config);
	}
}
