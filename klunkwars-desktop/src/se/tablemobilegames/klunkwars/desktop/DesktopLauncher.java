package se.tablemobilegames.klunkwars.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;

public class DesktopLauncher {
	public static void main(String[] arg) {
		final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1794;
		config.height = 1080;
		new LwjglApplication(new KlunkwarsGame(), config);
	}
}
