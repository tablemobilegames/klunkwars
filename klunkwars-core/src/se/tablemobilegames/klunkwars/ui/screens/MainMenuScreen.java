package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.services.SkinService;

public class MainMenuScreen implements Screen {

	private static final float PAD_BOTTOM = 20f;
	private static String COLOR_MENU_LABEL_HOVER = "#ff5102";
	private static String COLOR_MENU_LABEL = "#ffcb00";
	private final KlunkwarsGame game;
	private Label newGameLabel;
	private Label loadGameLabel;
	private Label settingsLabel;
	private Label exitLabel;
	private Table menuOptionsContainer;
	private final Stage stage;

	public MainMenuScreen(KlunkwarsGame game) {
		super();
		this.game = game;
		this.stage = new Stage(new FitViewport(game.getWidth(), game.getHeight()), this.game.getBatch());
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(this.stage);
		buildScreen();
	}

	private void buildScreen() {
		this.menuOptionsContainer = new Table();

		initNewGameOption();
		initLoadGameOption();
		initSettingsOption();
		initExitOption();

		this.stage.addActor(this.menuOptionsContainer);
		this.stage.addActor(this.newGameLabel);
		this.stage.addActor(this.loadGameLabel);
		this.stage.addActor(this.settingsLabel);
		this.stage.addActor(this.exitLabel);

		this.menuOptionsContainer.add(this.newGameLabel).padBottom(PAD_BOTTOM).row();
		this.menuOptionsContainer.add(this.loadGameLabel).padBottom(PAD_BOTTOM).row();
		this.menuOptionsContainer.add(this.settingsLabel).padBottom(PAD_BOTTOM).row();
		this.menuOptionsContainer.add(this.exitLabel);
		this.menuOptionsContainer.setFillParent(true);
		this.menuOptionsContainer.center();

	}

	private void initExitOption() {
		this.exitLabel = new Label("EXIT", SkinService.getUiSkin(), "large-font");
		this.exitLabel.setColor(Color.valueOf(COLOR_MENU_LABEL));
		this.exitLabel.addListener(new ClickListener() {

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				exitLabel.setColor(Color.valueOf(COLOR_MENU_LABEL_HOVER));
				super.enter(event, x, y, pointer, fromActor);
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				exitLabel.setColor(Color.valueOf(COLOR_MENU_LABEL));
				super.exit(event, x, y, pointer, toActor);
			}

			@Override
			public void clicked(InputEvent event, float x, float y) {
				Gdx.app.exit();
			}

		});
	}

	private void initSettingsOption() {
		this.settingsLabel = new Label("SETTINGS", SkinService.getUiSkin(), "large-font");
		this.settingsLabel.setColor(Color.valueOf(COLOR_MENU_LABEL));

	}

	private void initLoadGameOption() {
		this.loadGameLabel = new Label("LOAD GAME", SkinService.getUiSkin(), "large-font");
		this.loadGameLabel.setColor(Color.valueOf(COLOR_MENU_LABEL));
	}

	private void initNewGameOption() {
		this.newGameLabel = new Label("NEW GAME", SkinService.getUiSkin(), "large-font");
		this.newGameLabel.setColor(Color.valueOf(COLOR_MENU_LABEL));
		this.newGameLabel.addListener(new ClickListener() {

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				newGameLabel.setColor(Color.valueOf(COLOR_MENU_LABEL_HOVER));
				super.enter(event, x, y, pointer, fromActor);
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				newGameLabel.setColor(Color.valueOf(COLOR_MENU_LABEL));
				super.exit(event, x, y, pointer, toActor);
			}

			@Override
			public void clicked(InputEvent event, float x, float y) {
				game.changeScreenAtNewGame();
			}

		});

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		this.stage.act(delta);
		this.stage.draw();

	}

	@Override
	public void resize(int width, int height) {
		this.stage.getViewport().update(width, height, true);

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		this.stage.dispose();
	}

}
