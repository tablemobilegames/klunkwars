package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.common.PhaseEnum;
import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.klunkwars.services.PhaseService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;

public class TurnLobbyScreen implements Screen {

	private final KlunkwarsGame game;
	private final Stage stage;
	private final float GAME_WIDTH;
	private final float GAME_HEIGHT;
	private Table screenContainer;
	private Label phaseLabel;
	private Label playerLabel;
	private Label clickToStartLabel;
	private int numofTimeClicked;

	public TurnLobbyScreen(KlunkwarsGame game) {
		super();
		this.game = game;
		this.stage = new Stage(new FitViewport(this.game.getWidth(), this.game.getHeight()), this.game.getBatch());

		GAME_HEIGHT = game.getHeight();
		GAME_WIDTH = game.getWidth();
	}

	@Override
	public void show() {
		this.stage.clear();
		Gdx.input.setInputProcessor(this.stage);
		this.numofTimeClicked = 0;
		buildScreen();
	}

	private void buildScreen() {
		this.screenContainer = new Table();
		this.screenContainer.setFillParent(true);
		this.screenContainer.setPosition(0, 0);
		this.screenContainer.align(Align.center);

		if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_0) {
			Label newGameLabel = new Label("New game begins...", SkinService.getUiSkin(), "large-font");
			newGameLabel.setColor(Color.BLACK);

			clickToStartLabel = new Label("Click to start!", SkinService.getUiSkin(), "medium-font");
			clickToStartLabel.setColor(Color.BLACK);

			this.screenContainer.add(newGameLabel);
			this.screenContainer.row();
			this.screenContainer.add(clickToStartLabel);
		}

		if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_1) {
			//present cards for this round
			//deciders choose their decision
			clickToStartLabel = new Label("Click twice to start your turn", SkinService.getUiSkin(), "medium-font");
			clickToStartLabel.setColor(Color.BLACK);

			playerLabel = new Label("Next up to play: " + PlayerService.getCurrentPlayer().getName(),
					SkinService.getUiSkin(), "medium-font");
			playerLabel.setColor(Color.BLACK);

			this.screenContainer.add(phaseLabel);
			this.screenContainer.row();
			this.screenContainer.add(playerLabel);
			this.screenContainer.row();
			this.screenContainer.add(clickToStartLabel);

		}

		if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_2) {
			//present decision
			//all players choose role
			clickToStartLabel = new Label("Click twice to start your turn", SkinService.getUiSkin(), "medium-font");
			clickToStartLabel.setColor(Color.BLACK);

			playerLabel = new Label("Next up to play: " + PlayerService.getCurrentPlayer().getName(),
					SkinService.getUiSkin(), "medium-font");
			playerLabel.setColor(Color.BLACK);

			this.screenContainer.add(phaseLabel);
			this.screenContainer.row();
			this.screenContainer.add(playerLabel);
			this.screenContainer.row();
			this.screenContainer.add(clickToStartLabel);

		}

		this.screenContainer.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				numofTimeClicked += 1;

			}

		});

		stage.addActor(this.screenContainer);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		this.stage.act(delta);
		checkNumOfClick();
		this.stage.draw();
		this.stage.setDebugAll(false);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		this.stage.dispose();
	}

	private void checkNumOfClick() {
		if (numofTimeClicked == 1) {

			if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_0) {
				PhaseService.switchPhase();
				game.changeScreen(ScreenEnum.TURN_LOBBY);
			}

			else {
				this.clickToStartLabel.setText("Click once to start your turn");
			}
		}

		if (numofTimeClicked == 2 && PhaseService.getCurrentPhase() == PhaseEnum.PHASE_1) {
			game.changeScreen(ScreenEnum.PRESENT_CARDS);
		}

		if (numofTimeClicked == 2 && PhaseService.getCurrentPhase() == PhaseEnum.PHASE_2) {
			game.changeScreen(ScreenEnum.PLAY);
		}
	}

}
