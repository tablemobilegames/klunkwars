package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.ui.actors.ActionNavigator;
import se.tablemobilegames.klunkwars.ui.actors.ActionViewContainer;

public class PlayScreen extends PlayScreenBase implements Screen {

	public static float ACTION_CHOOSER_BAR_HEIGHT;
	public static float ACTION_CHOOSER_BAR_WIDTH;
	public static float ACTION_BUTTON_WIDTH;
	public static float ACTION_CHOOSER_BAR_X_POSITION;
	public static float ACTION_CHOOSER_BAR_Y_POSITION;

	private static float ACTION_VIEW_HEIGHT;
	private static float ACTION_VIEW_WIDTH;
	public static float ACTION_VIEW_X_POSITION;
	public static float ACTION_VIEW_Y_POSITION;

	private ActionNavigator actionNavigator;
	private ActionViewContainer actionView;

	public PlayScreen(KlunkwarsGame game) {

		super(game);

		//measure for Action chooser bar
		ACTION_CHOOSER_BAR_HEIGHT = GAME_HEIGHT / 8;
		ACTION_CHOOSER_BAR_WIDTH = GAME_WIDTH - PLAYERHOLDER_BAR_WIDTH;
		ACTION_BUTTON_WIDTH = ACTION_CHOOSER_BAR_WIDTH / 4;
		ACTION_CHOOSER_BAR_X_POSITION = PLAYERHOLDER_BAR__X_POSITION + PLAYERHOLDER_BAR_WIDTH;
		ACTION_CHOOSER_BAR_Y_POSITION = 0f;

		//measure for Action view bar
		ACTION_VIEW_X_POSITION = PLAYERHOLDER_BAR_WIDTH;
		ACTION_VIEW_Y_POSITION = ACTION_CHOOSER_BAR_HEIGHT;
		ACTION_VIEW_HEIGHT = GAME_HEIGHT - ACTION_CHOOSER_BAR_HEIGHT;
		ACTION_VIEW_WIDTH = GAME_WIDTH - PLAYERHOLDER_BAR_WIDTH;

	}

	/**
	 * GUI:
	 *
	 * ------------------------------------------------------------------------------------------------------
	 * -PlayerHolderBar - en ruta som h�ller alla spelare
	 *
	 * PlayerBlock - en ruta f�r spelaren med spelarens namn, avatarportr�tt,
	 * antal klunkstenar, tilldelade roller denna runda, tilldelad
	 * uppst�llningshandling denna runda
	 *
	 * ------------------------------------------------------------------------------------------------------
	 * KlunkResolverBar - en ruta som visar korten f�r m�ngd, r�knes�tt och
	 * spelare f�r denna runda. visar placeholders innan spelarna har valt kort.
	 *
	 * ------------------------------------------------------------------------------------------------------
	 * LogBar - en ruta som skriver ut statusuppdateringar i text f�r vad som
	 * h�nder i spelet. t ex vilka roller som v�ljs, sakkort som l�ggs, etc.
	 *
	 * ------------------------------------------------------------------------------------------------------
	 * ActionBar - en ruta som visar de handlingsalternativ som spelaren kan
	 * v�lja i detta skeende av spelet.
	 *
	 * GameTurnAction 1. Uppst�llningshandlingar, 2. v�lja roll och vad rollen
	 * ska g�ra, 3. visa resultat av rundan.
	 *
	 * ThingsAction - g� igenom sina sakkort och anv�nda dessa p� andra spelare.
	 *
	 * DrinkAction - v�lj att dricka valfritt antal klunkar om spelaren har
	 * klunkar.
	 *
	 * ------------------------------------------------------------------------------------------------------
	 * ActionView - n�r spelaren v�ljer ett handlingsalternativ s� presenteras
	 * info och fl�det f�r de valda handlingarna i detta f�nster
	 *
	 *
	 * ------------------------------------------------------------------------------------------------------
	 * GameMenu - g�r man in h�r s� har man settings, save game, load game, exit
	 *
	 *
	 * ------------------------------------------------------------------------------------------------------
	 *
	 **/

	@Override
	protected void buildMainContent() {
		mainContent = new Table();

		//ActionView
		actionView = new ActionViewContainer(this.game);
		actionView.setPosition(ACTION_VIEW_X_POSITION, ACTION_VIEW_Y_POSITION);
		actionView.setSize(ACTION_VIEW_WIDTH, ACTION_VIEW_HEIGHT);

		//ActionNavigator
		actionNavigator = new ActionNavigator(actionView, this.game);
		actionNavigator.setSize(PlayScreen.ACTION_CHOOSER_BAR_WIDTH, PlayScreen.ACTION_CHOOSER_BAR_HEIGHT);
		actionNavigator.setPosition(PlayScreen.ACTION_CHOOSER_BAR_X_POSITION, PlayScreen.ACTION_CHOOSER_BAR_Y_POSITION);

		mainContent.add(actionView);
		mainContent.row();
		mainContent.add(actionNavigator);

		this.stage.addActor(actionView);
		this.stage.addActor(actionNavigator);

	}

}