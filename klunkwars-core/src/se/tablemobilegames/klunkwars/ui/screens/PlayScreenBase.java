package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.ui.actors.PlayersStatusSideBar;

public abstract class PlayScreenBase implements Screen {

	protected final KlunkwarsGame game;
	protected final Stage stage;
	protected final float GAME_WIDTH;
	protected final float GAME_HEIGHT;

	protected static float PLAYERHOLDER_BAR_Y_POSITION;
	protected static float PLAYERHOLDER_BAR__X_POSITION;
	protected static float PLAYERHOLDER_BAR_WIDTH;
	protected static float PLAYERHOLDER_BAR_HEIGHT;

	private Table screenContainer;
	private Table sideBar;
	protected Table mainContent;
	private PlayersStatusSideBar playersBar;

	private Player currentPlayer;

	public PlayScreenBase(KlunkwarsGame game) {
		super();

		this.game = game;
		this.stage = new Stage(new FitViewport(this.game.getWidth(), this.game.getHeight()), this.game.getBatch());

		GAME_HEIGHT = game.getHeight();
		GAME_WIDTH = game.getWidth();

		//measure for Player holder bar
		PLAYERHOLDER_BAR_Y_POSITION = 0f;
		PLAYERHOLDER_BAR__X_POSITION = 0f;
		PLAYERHOLDER_BAR_WIDTH = GAME_WIDTH / 4;
		PLAYERHOLDER_BAR_HEIGHT = GAME_HEIGHT * 0.9f;

	}

	@Override
	public void show() {
		this.stage.clear();
		Gdx.input.setInputProcessor(this.stage);
		currentPlayer = PlayerService.getCurrentPlayer();
		buildScreen();

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		this.stage.act(delta);
		this.stage.draw();
		this.stage.setDebugAll(false);

	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		this.stage.dispose();

	}

	private void buildScreen() {

		this.screenContainer = new Table();
		this.screenContainer.setFillParent(true);

		buildSideBar();
		buildMainContent();

		this.screenContainer.add(sideBar);
		this.screenContainer.add(mainContent);

		this.stage.addActor(this.screenContainer);
		this.stage.addActor(sideBar);
		this.stage.addActor(mainContent);

	}

	/**
	 * Override this method
	 **/
	protected void buildMainContent() {

	}

	private void buildSideBar() {
		sideBar = new Table();
		sideBar.top();

		//PlayersBar
		playersBar = new PlayersStatusSideBar();
		playersBar.setSize(PLAYERHOLDER_BAR_WIDTH, PLAYERHOLDER_BAR_HEIGHT);
		playersBar.setPosition(PLAYERHOLDER_BAR__X_POSITION, PLAYERHOLDER_BAR_Y_POSITION);
		playersBar.top();

		sideBar.add(playersBar);

		this.stage.addActor(playersBar);

	}

}
