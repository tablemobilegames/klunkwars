package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.klunkwars.services.KlunkResolverService;
import se.tablemobilegames.klunkwars.services.PhaseService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.RulesService;
import se.tablemobilegames.klunkwars.services.SkinService;

public class ResultScreen implements Screen {

	private final KlunkwarsGame game;
	private final Stage stage;
	private Table screenContainer;
	private boolean endGame = false;
	private TextButton okButton;

	public ResultScreen(KlunkwarsGame game) {
		super();
		this.game = game;
		this.stage = new Stage(new FitViewport(this.game.getWidth(), this.game.getHeight()), this.game.getBatch());
	}

	@Override
	public void show() {
		this.stage.clear();
		Gdx.input.setInputProcessor(this.stage);
		buildScreen();

	}

	private void buildScreen() {
		this.screenContainer = new Table();
		this.screenContainer.setFillParent(true);
		this.screenContainer.setPosition(0, 0);
		this.screenContainer.align(Align.center);

		Label resultLabel = new Label("S�h�r blev det... " + KlunkResolverService.presentResult(),
				SkinService.getUiSkin(), "medium-font");
		resultLabel.setColor(Color.BLACK);

		Label defeatedPlayersLabel = null;
		String defeatedPlayers = RulesService.presentDefeatedPlayersThisRound();
		if (!defeatedPlayers.isEmpty()) {
			defeatedPlayersLabel = new Label(defeatedPlayers, SkinService.getUiSkin(), "small-font");
			defeatedPlayersLabel.setColor(Color.BLACK);
		}

		if (PlayerService.getActivePlayers().size() == 1) {
			endGame = true;
		}

		if (!endGame) {
			okButton = new TextButton("OK", SkinService.getUiSkin(), "default");
			okButton.addListener(new ClickListener() {

				@Override
				public void clicked(InputEvent event, float x, float y) {
					PhaseService.switchPhase();
					game.changeScreen(ScreenEnum.TURN_LOBBY);
					super.clicked(event, x, y);
				}

			});
		}

		this.screenContainer.add(resultLabel).padBottom(10f);
		this.screenContainer.row();
		if (!endGame) {
			this.screenContainer.add(okButton).padBottom(10f);
		}
		if (defeatedPlayersLabel != null) {
			this.screenContainer.row();
			this.screenContainer.add(defeatedPlayersLabel);
		}

		stage.addActor(this.screenContainer);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		this.stage.act(delta);
		this.stage.draw();
		this.stage.setDebugAll(false);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		this.stage.dispose();
	}

}
