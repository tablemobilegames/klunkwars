package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.klunkwars.services.SkinService;

public class LoadingScreen implements Screen {

	private final ShapeRenderer shapeRenderer;
	private float progress;
	private final KlunkwarsGame game;
	private final AssetManager assets;

	public LoadingScreen(KlunkwarsGame game) {
		this.game = game;
		this.shapeRenderer = new ShapeRenderer();
		this.assets = game.getAssetManager();
	}

	private void queueAssets() {
		assets.load("ui/uiskin.atlas", TextureAtlas.class);
		assets.load("avatars/avatarskin.atlas", TextureAtlas.class);
		assets.load("amounts/amount.atlas", TextureAtlas.class);
		assets.load("arithmetics/arithmetics.atlas", TextureAtlas.class);
	}

	@Override
	public void show() {
		System.out.println("LOADING");
		shapeRenderer.setProjectionMatrix(this.game.getCamera().combined);
		this.progress = 0f;
		queueAssets();

	}

	private void update(float delta) {
		progress = MathUtils.lerp(progress, this.assets.getProgress(), .1f);
		if (this.assets.update() && progress >= this.assets.getProgress() - .001f) {
			SkinService.initSkins(this.game);
			game.changeScreen(ScreenEnum.MAIN_MENU);
		}
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		update(delta);

		shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
		shapeRenderer.setColor(Color.BLACK);
		shapeRenderer.rect(32, game.getCamera().viewportHeight / 2 - 8, game.getCamera().viewportWidth - 64, 16);

		shapeRenderer.setColor(Color.BLUE);
		shapeRenderer.rect(32, game.getCamera().viewportHeight / 2 - 8,
				progress * (game.getCamera().viewportWidth - 64), 16);
		shapeRenderer.end();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		shapeRenderer.dispose();

	}

}
