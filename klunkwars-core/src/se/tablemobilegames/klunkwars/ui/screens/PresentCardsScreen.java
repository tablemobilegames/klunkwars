package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.KlunkResolverService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;
import se.tablemobilegames.klunkwars.ui.actors.PlayerCardsBlock;

public class PresentCardsScreen implements Screen {

	private final KlunkwarsGame game;
	private final Stage stage;
	private final float GAME_WIDTH;
	private final float GAME_HEIGHT;
	private Table screenContainer;
	private Label headerLabel;
	private Label descLabel;
	private TextButton startGameButton;

	public PresentCardsScreen(KlunkwarsGame game) {
		super();
		this.game = game;
		this.stage = new Stage(new FitViewport(this.game.getWidth(), this.game.getHeight()), this.game.getBatch());

		GAME_HEIGHT = game.getHeight();
		GAME_WIDTH = game.getWidth();
	}

	@Override
	public void show() {
		this.stage.clear();
		Gdx.input.setInputProcessor(this.stage);
		buildScreen();
	}

	private void buildScreen() {

		this.screenContainer = new Table();
		this.screenContainer.setFillParent(true);
		this.screenContainer.setPosition(0, 0);
		this.screenContainer.align(Align.top);

		initStartGameButton();

		headerLabel = new Label("CARDS FOR THIS ROUND", SkinService.getUiSkin(), "large-font");
		headerLabel.setColor(Color.BLACK);

		Table playersCardContainer = new Table();

		for (int i = 1; i <= PlayerService.getActivePlayers().size(); i++) {
			Player player = PlayerService.getActivePlayers().get(i - 1);
			PlayerCardsBlock block = new PlayerCardsBlock(player);
			playersCardContainer.add(block).width(GAME_WIDTH / 4).pad(2f);
			if (i % 3 == 0) {
				playersCardContainer.row();
			}
		}

		this.screenContainer.add(headerLabel).padBottom(15f).padTop(20f).expandX();
		this.screenContainer.row();
		checkAddTwoPlayerLabel();
		this.screenContainer.add(playersCardContainer).expandX().align(Align.center).padBottom(20f);
		this.screenContainer.row();
		this.screenContainer.add(startGameButton);

		stage.addActor(this.screenContainer);

	}

	private void checkAddTwoPlayerLabel() {

		if (PlayerService.getActivePlayers().size() <= 2) {
			descLabel = new Label(
					"With only two players Arithmetic is set randomly. This time you get: "
							+ KlunkResolverService.getKlunkDecision().getArithmetic().getValue(),
					SkinService.getUiSkin(), "small-font");
			descLabel.setColor(Color.RED);

			this.screenContainer.add(descLabel).padBottom(20f).expandX();
			this.screenContainer.row();
		}
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		this.stage.act(delta);
		this.stage.draw();
		this.stage.setDebugAll(false);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		this.stage.dispose();
	}

	private void initStartGameButton() {
		startGameButton = new TextButton("START GAME", SkinService.getUiSkin(), "default");
		startGameButton.getLabelCell().size(((GAME_WIDTH / 6) - 4), GAME_HEIGHT / 20);
		startGameButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				game.changeScreen(ScreenEnum.PLAY);
			}

		});

	}

}
