package se.tablemobilegames.klunkwars.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;
import se.tablemobilegames.klunkwars.ui.actors.Slot;

public class SetupNewGameScreen implements Screen {

	private static final int NUM_OF_SLOTS = 6;
	private static final int MINIMUM_PLAYER_COUNT = 2;
	private final KlunkwarsGame game;
	private final Stage stage;

	private final float GAME_WIDTH;
	private final float GAME_HEIGHT;
	private final float SCREEN_HEADLINE_HEIGHT;
	private final float SCREEN_HEADLINE_WIDTH;

	private boolean startEnabled;

	private Table screenContainer;
	private Table playersContainer;
	private Table optionsContainer;

	private TextButton startGameButton;
	private TextButton returnButton;

	public SetupNewGameScreen(KlunkwarsGame game) {
		super();

		this.game = game;
		this.stage = new Stage(new FitViewport(game.getWidth(), game.getHeight()), this.game.getBatch());

		GAME_WIDTH = this.game.getWidth();
		GAME_HEIGHT = this.game.getHeight();

		//measure headline container
		SCREEN_HEADLINE_HEIGHT = GAME_HEIGHT * 0.90f;
		SCREEN_HEADLINE_WIDTH = GAME_WIDTH;
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(this.stage);
		this.stage.clear();
		PlayerService.resetPlayerList();
		this.startEnabled = false;
		buildScreen();
	}

	private void buildScreen() {
		screenContainer = new Table();
		screenContainer.setFillParent(true);
		screenContainer.setPosition(0, 0);
		screenContainer.align(Align.top);

		playersContainer = new Table();
		optionsContainer = new Table();

		final Label headerLabel = new Label("GAME LOBBY", SkinService.getUiSkin(), "large-font");
		headerLabel.setColor(Color.BLACK);

		initStartGameButton();
		initReturnButton();
		buildPlayerSlots(playersContainer);
		buildOptionsContainer();

		this.stage.addActor(screenContainer);
		this.stage.addActor(playersContainer);
		this.stage.addActor(optionsContainer);
		this.stage.addActor(headerLabel);

		screenContainer.add(headerLabel).padBottom(30f).padTop(20f).expandX();
		screenContainer.row();
		screenContainer.add(playersContainer).width(GAME_WIDTH - (GAME_WIDTH / 5)).expandX().align(Align.left);
		screenContainer.add(optionsContainer).width(GAME_WIDTH / 5).height(GAME_HEIGHT / 2.24f).align(Align.bottom)
				.padRight(10);

	}

	private void buildOptionsContainer() {
		optionsContainer.add(startGameButton).expandX().height(GAME_HEIGHT / 6).padBottom(20);
		optionsContainer.row();
		optionsContainer.add(returnButton).expandX().height(GAME_HEIGHT / 6).align(Align.bottom);
	}

	private void buildPlayerSlots(Table playersContainer) {
		for (int i = 1; i <= NUM_OF_SLOTS; i++) {
			final Slot slot = new Slot();
			this.stage.addActor(slot);
			playersContainer.add(slot).width(GAME_WIDTH / 4).pad(10f).padBottom(50f);
			if (i % 3 == 0) {
				playersContainer.row();
			}
		}
	}

	private void initReturnButton() {
		returnButton = new TextButton("RETURN", SkinService.getUiSkin(), "default");
		returnButton.getLabelCell().size(((GAME_WIDTH / 6) - 4), GAME_HEIGHT / 20f);
		returnButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				game.changeScreen(ScreenEnum.MAIN_MENU);
			}
		});
	}

	private void initStartGameButton() {
		startGameButton = new TextButton("START GAME", SkinService.getUiSkin(), "default");
		startGameButton.getLabelCell().size(((GAME_WIDTH / 6) - 4), GAME_HEIGHT / 20);
		startGameButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				if (!startGameButton.isDisabled()) {
					startGame();
				}
			}
		});

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f, 1f, 1f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		this.stage.act(delta);
		checkStartEnabled();
		this.stage.draw();
		this.stage.setDebugAll(false);

	}

	private void checkStartEnabled() {
		if (PlayerService.getAllPlayers().size() < MINIMUM_PLAYER_COUNT) {
			startEnabled = false;
			startGameButton.setColor(Color.RED);
		}
		else {
			startEnabled = true;
			startGameButton.setColor(Color.WHITE);
		}
		startGameButton.setDisabled(!startEnabled); //button locked before 2 players are added

	}

	@Override
	public void resize(int width, int height) {
		this.stage.getViewport().update(width, height, true);

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		this.stage.dispose();

	}

	private void startGame() {
		for (final Player player : PlayerService.getAllPlayers()) {
			player.setInPlay(true);
		}

		game.changeScreen(ScreenEnum.TURN_LOBBY);
	}

}
