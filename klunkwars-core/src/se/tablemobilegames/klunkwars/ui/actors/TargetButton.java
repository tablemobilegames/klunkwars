package se.tablemobilegames.klunkwars.ui.actors;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import se.tablemobilegames.klunkwars.modelobjects.Player;

public class TargetButton extends ImageButton {

	private Player player;
	private boolean selected;

	public TargetButton(Drawable imageUp, Drawable imageDown, Drawable imageChecked, Player player) {
		super(imageUp, imageDown, imageChecked);
		this.player = player;
		this.selected = false;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
