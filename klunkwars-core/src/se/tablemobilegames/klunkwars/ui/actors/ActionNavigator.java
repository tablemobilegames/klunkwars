package se.tablemobilegames.klunkwars.ui.actors;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.common.ActionChoiceEnum;
import se.tablemobilegames.klunkwars.common.KlunkDecisionRoleEnum;
import se.tablemobilegames.klunkwars.common.PhaseEnum;
import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.PhaseService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;
import se.tablemobilegames.klunkwars.services.TurnService;
import se.tablemobilegames.klunkwars.ui.screens.PlayScreen;

public class ActionNavigator extends Table {

	private ActionViewContainer actionView;
	private KlunkwarsGame game;

	private ButtonGroup<Button> buttongroup;
	private TextButton endTurnActionButton;
	private TextButton drinkActionButton;
	private TextButton thingsActionButton;
	private TextButton gameTurnActionButton;

	public ActionNavigator(ActionViewContainer actionView, KlunkwarsGame game) {
		super();
		this.actionView = actionView;
		this.game = game;

		buildBar();
	}

	private void buildBar() {
		buttongroup = new ButtonGroup<Button>();
		buttongroup.setMaxCheckCount(1);

		if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_1) {
			initKlunkDecisionButton(PlayerService.getCurrentPlayer());
		}

		initThingsActionButton();
		initDrinkActionButton();
		initEndTurnButton();

		buttongroup.add(thingsActionButton);
		buttongroup.add(drinkActionButton);
		buttongroup.add(endTurnActionButton);

		this.add(thingsActionButton);
		this.add(drinkActionButton);
		this.add(endTurnActionButton);

	}

	private void initEndTurnButton() {

		endTurnActionButton = new TextButton("Avsluta rundan", SkinService.getUiSkin(), "actionbar");
		endTurnActionButton.getLabelCell().size(PlayScreen.ACTION_BUTTON_WIDTH, PlayScreen.ACTION_CHOOSER_BAR_HEIGHT);
		endTurnActionButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);

				if (TurnService.getAllowEndTurn()) {
					TurnService.endPlayerTurn();
					TurnService.setAllowEndTurn(false);

					if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_1) {
						System.out.println("fas 1");
						game.changeScreen(ScreenEnum.TURN_LOBBY);
					}

					if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_2) {
						System.out.println("fas 2");
						game.changeScreen(ScreenEnum.TURN_LOBBY);
					}

					if (PhaseService.getCurrentPhase() == PhaseEnum.PHASE_3) {
						System.out.println("fas 3");
						game.changeScreen(ScreenEnum.RESULT);
					}

				}
				else {
					System.out.println("Du f�r inte avsluta rundan �n");
				}
			}
		});

	}

	private void initDrinkActionButton() {
		drinkActionButton = new TextButton("Drick klunkar", SkinService.getUiSkin(), "actionbar");
		drinkActionButton.getLabelCell().size(PlayScreen.ACTION_BUTTON_WIDTH, PlayScreen.ACTION_CHOOSER_BAR_HEIGHT);
		drinkActionButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				actionView.changeActionView(ActionChoiceEnum.DRINK);
				super.clicked(event, x, y);
			}

		});

	}

	private void initThingsActionButton() {
		thingsActionButton = new TextButton("Grejer", SkinService.getUiSkin(), "actionbar");
		thingsActionButton.getLabelCell().size(PlayScreen.ACTION_BUTTON_WIDTH, PlayScreen.ACTION_CHOOSER_BAR_HEIGHT);
		thingsActionButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				actionView.changeActionView(ActionChoiceEnum.THINGS);
				super.clicked(event, x, y);
			}

		});

	}

	private void initKlunkDecisionButton(Player currentPlayer) {
		if (currentPlayer.getDecisionRole() != KlunkDecisionRoleEnum.NoDecider) {
			gameTurnActionButton = new TextButton(currentPlayer.getDecisionRole().getValue(), SkinService.getUiSkin(),
					"actionbar");
			gameTurnActionButton.getLabelCell().size(PlayScreen.ACTION_BUTTON_WIDTH,
					PlayScreen.ACTION_CHOOSER_BAR_HEIGHT);
			gameTurnActionButton.addListener(new ClickListener() {

				@Override
				public void clicked(InputEvent event, float x, float y) {
					actionView.changeActionView(ActionChoiceEnum.GAMETURN_ACTION);
					super.clicked(event, x, y);
				}
			});

			buttongroup.add(gameTurnActionButton);
			this.add(gameTurnActionButton);
		}

	}

}
