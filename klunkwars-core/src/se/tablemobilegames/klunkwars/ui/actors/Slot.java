package se.tablemobilegames.klunkwars.ui.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.AvatarService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;

public class Slot extends Stack {

	private final Table activeSlot;
	private final Table emptySlot;
	private Player player;
	private Label playerName;
	private Image avatarImg;
	private Label title;

	public Slot() {
		super();
		activeSlot = buildActiveSlot();
		emptySlot = buildEmptySlot();
		this.add(activeSlot);
		this.add(emptySlot);
		deactivateSlot();
	}

	private Table buildEmptySlot() {
		final Table table = new Table();
		final Label title = new Label("EMPTY SLOT", SkinService.getUiSkin(), "medium-font");
		title.setColor(Color.BLACK);
		final TextButton addPlayer = new TextButton("ADD PLAYER", SkinService.getUiSkin(), "setupgame");
		addPlayer.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				activateSlot(new Player());
			}
		});

		table.add(title).padBottom(5f);
		table.row();
		table.add(addPlayer);

		return table;
	}

	private Table buildActiveSlot() {

		final Table table = new Table();
		title = new Label("PLAYER", SkinService.getUiSkin(), "medium-font");
		title.setColor(Color.BLACK);
		playerName = new Label("spelarens namn", SkinService.getUiSkin(), "medium-font");
		playerName.setColor(Color.BLACK);
		final Texture avatarTexture = new Texture(Gdx.files.internal("avatars/Aldo_100.jpg"));
		avatarImg = new Image(avatarTexture);
		final TextButton customizeButton = new TextButton("CUSTOMIZE", SkinService.getUiSkin(), "setupgame");
		final TextButton removeButton = new TextButton("Remove Player", SkinService.getUiSkin(), "setupgame");

		removeButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				deactivateSlot();
			}
		});

		table.add(title).padBottom(5f);
		table.row();
		table.add(playerName).padBottom(15f);
		table.row();
		table.add(avatarImg).padBottom(10f);
		table.row();
		table.add(customizeButton).padBottom(10f);
		table.row();
		table.add(removeButton);

		return table;
	}

	public void activateSlot(Player playerToActivate) {
		player = playerToActivate;
		player.setAvatar(AvatarService.getRandomAvatarToUse());
		PlayerService.addPlayer(player);
		player.setName(player.getAvatar().getDefaultName());
		playerName.setText(player.getName());
		avatarImg.setDrawable(player.getAvatar().getDefaultDrawable());
		this.emptySlot.setVisible(false);
		this.activeSlot.setVisible(true);
	}

	public void deactivateSlot() {
		if (player != null) {
			PlayerService.removePlayer(player);
			player.getAvatar().setUsed(false);
			player = null;
		}
		this.activeSlot.setVisible(false);
		this.emptySlot.setVisible(true);
	}

}
