package se.tablemobilegames.klunkwars.ui.actors;

import java.util.List;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.PlayerService;

public class PlayersStatusSideBar extends Table {

	public PlayersStatusSideBar() {
		super();
		buildBar();
	}

	private void buildBar() {

		List<Player> sortedPlayers = PlayerService.getActivePlayersSortedByTurnNumber();

		for (Player player : sortedPlayers) {
			this.add(new PlayerStatusBlock(player)).padBottom(20);
			this.row();
		}
	}

}
