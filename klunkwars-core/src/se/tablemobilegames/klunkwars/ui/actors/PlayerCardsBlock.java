package se.tablemobilegames.klunkwars.ui.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import se.tablemobilegames.klunkwars.common.KlunkDecisionRoleEnum;
import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.SkinService;

public class PlayerCardsBlock extends Table {

	private Player player;

	private Label playerName;
	private Image avatarImg;
	private Label title;
	private Label roles;

	public PlayerCardsBlock(Player player) {
		super();

		this.player = player;

		buildBlock();
	}

	private void buildBlock() {

		title = new Label("PLAYER " + decisionText(), SkinService.getUiSkin(), "small-font");
		title.setColor(Color.BLACK);
		playerName = new Label(player.getName(), SkinService.getUiSkin(), "small-font");
		playerName.setColor(Color.BLACK);
		final Drawable avatarTexture = player.getAvatar().getDefaultDrawable();
		avatarImg = new Image(avatarTexture);
		roles = new Label("Roller: L�kare & Amat�r", SkinService.getUiSkin(), "small-font");
		roles.setColor(Color.BLACK);

		this.pad(14f);
		this.add(title).padBottom(5f);
		this.row();
		this.add(playerName).padBottom(15f);
		this.row();
		this.add(avatarImg).padBottom(10f);
		this.row();
		this.add(roles).padBottom(10f);
	}

	private String decisionText() {
		String text = "";
		if (player.getDecisionRole() != KlunkDecisionRoleEnum.NoDecider) {
			text = "(" + player.getDecisionRole().getValue() + ")";
		}

		return text;
	}

}
