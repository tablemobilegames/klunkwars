package se.tablemobilegames.klunkwars.ui.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;

import se.tablemobilegames.klunkwars.common.KlunkObserver;
import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.ObserverService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;

public class PlayerStatusBlock extends Table implements KlunkObserver {

	private final Player player;
	private final Label headlineLabel;
	private final Table infoTable;
	private final Image playerPortrait;
	private final Label klunkStonesLabel;
	private final Label rolesLabel;

	public PlayerStatusBlock(Player player) {
		super();
		attachKlunkObserver();
		this.player = player;

		//set background
		final Pixmap pm1 = new Pixmap(1, 1, Format.RGB565);
		if (this.player == PlayerService.getCurrentPlayer()) {
			pm1.setColor(Color.TAN);
		}
		else {
			pm1.setColor(Color.LIGHT_GRAY);
		}

		pm1.fill();
		this.setBackground(new TextureRegionDrawable(new TextureRegion(new Texture(pm1))));

		this.headlineLabel = new Label(this.player.getName(), SkinService.getUiSkin(), "tiny-font");
		this.headlineLabel.setColor(Color.BLACK);
		this.infoTable = new Table();
		this.playerPortrait = new Image(player.getAvatar().getDefaultDrawable());
		this.klunkStonesLabel = new Label("Klunkstones: " + this.player.getKlunkStones(), SkinService.getUiSkin(),
				"tiny-font");
		this.klunkStonesLabel.setColor(Color.BLACK);
		this.rolesLabel = new Label("Roller: L�kare, Amat�r", SkinService.getUiSkin(), "tiny-font");
		this.rolesLabel.setColor(Color.BLACK);

		buildBlock();
	}

	private void buildBlock() {

		//build infotable
		this.infoTable.add(this.headlineLabel).align(Align.left).pad(4);
		this.infoTable.row();
		this.infoTable.add(this.klunkStonesLabel).align(Align.left).pad(4);
		this.infoTable.row();
		this.infoTable.add(this.rolesLabel).align(Align.left).pad(4);

		this.add(this.playerPortrait);
		this.add(this.infoTable);
	}

	@Override
	public void update() {
		this.klunkStonesLabel.setText("Klunkstones: " + this.player.getKlunkStones());

	}

	@Override
	public void attachKlunkObserver() {
		ObserverService.attach(this);
	}

}
