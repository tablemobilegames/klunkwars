package se.tablemobilegames.klunkwars.ui.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.klunkwars.common.ActionChoiceEnum;
import se.tablemobilegames.klunkwars.common.KlunkDecisionRoleEnum;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;
import se.tablemobilegames.klunkwars.ui.actionviews.ActionView;
import se.tablemobilegames.klunkwars.ui.actionviews.AmountView;
import se.tablemobilegames.klunkwars.ui.actionviews.ArithmeticView;
import se.tablemobilegames.klunkwars.ui.actionviews.DrinkView;
import se.tablemobilegames.klunkwars.ui.actionviews.TargetsView;
import se.tablemobilegames.klunkwars.ui.screens.PlayScreen;

public class ActionViewContainer extends Stack {

	private KlunkwarsGame game;
	private ActionView actionview;
	private Group thingsView;
	private DrinkView drinkView;
	private KlunkDecisionRoleEnum currentRole;

	public ActionViewContainer(KlunkwarsGame game) {
		super();
		this.game = game;

		buildView();
	}

	private void buildView() {

		currentRole = PlayerService.getCurrentPlayer().getDecisionRole();
		buildViewByCurrentDeciderRole();
		buildThingsView();

		drinkView = new DrinkView(this.game, PlayScreen.ACTION_VIEW_X_POSITION, PlayScreen.ACTION_VIEW_Y_POSITION);
		drinkView.setVisible(false);
		this.add(drinkView);

	}

	private void buildThingsView() {
		thingsView = new Group();
		final Label testlabel2 = new Label("saker h�r", SkinService.getUiSkin(), "default");
		testlabel2.setColor(Color.BLACK);
		thingsView.addActor(testlabel2);
		thingsView.setVisible(false);
		this.add(thingsView);
	}

	private void buildViewByCurrentDeciderRole() {

		if (currentRole == KlunkDecisionRoleEnum.Amount) {
			actionview = new AmountView(this.game, PlayScreen.ACTION_VIEW_X_POSITION,
					PlayScreen.ACTION_VIEW_Y_POSITION);
		}

		if (currentRole == KlunkDecisionRoleEnum.Arithmetic) {
			actionview = new ArithmeticView(this.game, PlayScreen.ACTION_VIEW_X_POSITION,
					PlayScreen.ACTION_VIEW_Y_POSITION);
		}

		if (currentRole == KlunkDecisionRoleEnum.Targets) {
			actionview = new TargetsView(this.game, PlayScreen.ACTION_VIEW_X_POSITION,
					PlayScreen.ACTION_VIEW_Y_POSITION);
		}

		if (actionview != null) {
			actionview.setVisible(false);
			this.add((Actor) actionview);
		}
	}

	public void changeActionView(ActionChoiceEnum chosenView) {

		switch (chosenView) {
		case GAMETURN_ACTION:
			showView((Actor) actionview);
			break;

		case THINGS:
			showView(thingsView);
			break;

		case DRINK:
			showView(drinkView);
			break;

		}
	}

	private void showView(Actor view) {

		for (final Actor actor : this.getChildren()) {
			if (actor.equals(view)) {
				actor.setVisible(true);
			}
			else {
				actor.setVisible(false);
			}
		}
	}

}
