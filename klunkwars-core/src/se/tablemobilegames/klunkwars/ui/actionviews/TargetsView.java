package se.tablemobilegames.klunkwars.ui.actionviews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.KlunkResolverService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.TurnService;
import se.tablemobilegames.klunkwars.ui.actors.TargetButton;
import se.tablemobilegames.platform.interfaces.TableMobileGame;

public class TargetsView extends Table implements ActionView {

	private final float VIEW_X_POSITION;
	private final float VIEW_Y_POSITION;

	private ButtonGroup<ImageButton> buttongroup = new ButtonGroup<ImageButton>();
	private Table tablesContainer;

	public TargetsView(TableMobileGame game, float VIEW_X_POSITION, float VIEW_Y_POSITION) {
		super();

		this.VIEW_X_POSITION = VIEW_X_POSITION;
		this.VIEW_Y_POSITION = VIEW_Y_POSITION;

		buildView();

	}

	private void buildView() {

		tablesContainer = new Table();
		buttongroup = new ButtonGroup<ImageButton>();

		for (int i = 0; i < PlayerService.getActivePlayers().size(); i++) {

			Player currentPlayer = PlayerService.findNextPlayerInTurn(i);

			Drawable up = currentPlayer.getAvatar().getBwDrawable();
			Drawable down = currentPlayer.getAvatar().getDefaultDrawable();
			final TargetButton targetButton = new TargetButton(up, up, down, currentPlayer);

			targetButton.addListener(new ClickListener() {

				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);

					if (targetButton.isSelected()) {
						targetButton.setSelected(false);
						KlunkResolverService.getKlunkDecision().removeTarget(targetButton.getPlayer());
						checkAllowEndTurn();
					}
					else {
						targetButton.setSelected(true);
						KlunkResolverService.getKlunkDecision().addTarget(targetButton.getPlayer());
						checkAllowEndTurn();
					}
				}

			});

			if (i % 3 == 0) {
				tablesContainer.row();
			}

			buttongroup.add(targetButton);
			tablesContainer.add(targetButton).pad(10f).padBottom(20f).padRight(10f);

		}

		buttongroup.setMaxCheckCount(KlunkResolverService.getNumOfTargetsToPick());
		buttongroup.setMinCheckCount(0);
		buttongroup.uncheckAll();

		this.add(tablesContainer).center();
	}

	public void checkAllowEndTurn() {

		if (buttongroup.getAllChecked().size == KlunkResolverService.getNumOfTargetsToPick()) {
			TurnService.setAllowEndTurn(true);
		}
		else {
			TurnService.setAllowEndTurn(false);
		}
	}

}
