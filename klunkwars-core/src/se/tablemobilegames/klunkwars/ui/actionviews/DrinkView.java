package se.tablemobilegames.klunkwars.ui.actionviews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import se.tablemobilegames.klunkwars.modelobjects.Player;
import se.tablemobilegames.klunkwars.services.ObserverService;
import se.tablemobilegames.klunkwars.services.PlayerService;
import se.tablemobilegames.klunkwars.services.SkinService;
import se.tablemobilegames.platform.interfaces.TableMobileGame;

public class DrinkView extends Table implements ActionView {

	private Label testlabel3;
	private final float VIEW_X_POSITION;
	private final float VIEW_Y_POSITION;

	private Player currentPlayer = PlayerService.getCurrentPlayer();

	public DrinkView(TableMobileGame game, float VIEW_X_POSITION, float VIEW_Y_POSITION) {
		super();

		this.VIEW_X_POSITION = VIEW_X_POSITION;
		this.VIEW_Y_POSITION = VIEW_Y_POSITION;

		buildView();

	}

	private void buildView() {

		final TextButton drinkButton = new TextButton(currentPlayer.getKlunkStones() + " klunks left to drink",
				SkinService.getUiSkin(), "default");

		drinkButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				currentPlayer.removeKlunkStones(1);
				drinkButton.setText(currentPlayer.getKlunkStones() + " klunks left to drink");
				ObserverService.updateKlunks();
			}

		});

		this.add(drinkButton).center();

	}

}
