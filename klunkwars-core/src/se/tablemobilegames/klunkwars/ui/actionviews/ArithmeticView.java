package se.tablemobilegames.klunkwars.ui.actionviews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import se.tablemobilegames.klunkwars.common.ArithmeticEnum;
import se.tablemobilegames.klunkwars.services.KlunkResolverService;
import se.tablemobilegames.klunkwars.services.SkinService;
import se.tablemobilegames.klunkwars.services.TurnService;
import se.tablemobilegames.platform.interfaces.TableMobileGame;

public class ArithmeticView extends Table implements ActionView {

	private Label testlabel3;
	private final float VIEW_X_POSITION;
	private final float VIEW_Y_POSITION;

	private ButtonGroup<ImageButton> buttongroup = new ButtonGroup<ImageButton>();
	Table tablesContainer;

	public ArithmeticView(TableMobileGame game, float VIEW_X_POSITION, float VIEW_Y_POSITION) {
		super();

		this.VIEW_X_POSITION = VIEW_X_POSITION;
		this.VIEW_Y_POSITION = VIEW_Y_POSITION;

		buildView();

	}

	private void buildView() {
		buttongroup = new ButtonGroup<ImageButton>();
		tablesContainer = new Table();

		ImageButton imageButtonPlus;
		Drawable plusup = SkinService.getArithmeticSkin().getDrawable("plusup");
		Drawable plusdown = SkinService.getArithmeticSkin().getDrawable("plusdown");
		imageButtonPlus = new ImageButton(plusup, plusup, plusdown);

		imageButtonPlus.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				KlunkResolverService.getKlunkDecision().setArithmetic(ArithmeticEnum.PLUS);
				checkAllowEndTurn();
			}

		});

		ImageButton imageButtonMinus;
		Drawable minusup = SkinService.getArithmeticSkin().getDrawable("minusup");
		Drawable minusdown = SkinService.getArithmeticSkin().getDrawable("minusdown");
		imageButtonMinus = new ImageButton(minusup, minusup, minusdown);

		imageButtonMinus.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				KlunkResolverService.getKlunkDecision().setArithmetic(ArithmeticEnum.MINUS);
				checkAllowEndTurn();
			}

		});

		buttongroup.setMaxCheckCount(1);
		buttongroup.setMinCheckCount(0);
		buttongroup.uncheckAll();

		buttongroup.add(imageButtonPlus);
		buttongroup.add(imageButtonMinus);

		tablesContainer.add(imageButtonPlus).size(200f, 200f).pad(10f).padRight(200f);
		tablesContainer.add(imageButtonMinus).size(200f, 200f).pad(10f);

		this.add(tablesContainer).center();
	}

	public void checkAllowEndTurn() {

		if (buttongroup.getChecked() != null) {
			TurnService.setAllowEndTurn(true);
		}
		else {
			TurnService.setAllowEndTurn(false);
		}
	}

}
