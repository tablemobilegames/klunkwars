package se.tablemobilegames.klunkwars.ui.actionviews;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import se.tablemobilegames.klunkwars.services.KlunkResolverService;
import se.tablemobilegames.klunkwars.services.SkinService;
import se.tablemobilegames.klunkwars.services.TurnService;
import se.tablemobilegames.platform.interfaces.TableMobileGame;

public class AmountView extends Table implements ActionView {

	private final float VIEW_X_POSITION;
	private final float VIEW_Y_POSITION;

	private ButtonGroup<ImageButton> buttongroup1 = new ButtonGroup<ImageButton>();
	private ButtonGroup<ImageButton> buttongroup2 = new ButtonGroup<ImageButton>();
	Table tablesContainer;
	Table firstAmountTable;
	Table secondAmountTable;
	Label firstTableHeader;
	Label secondTableHeader;

	public AmountView(TableMobileGame game, float VIEW_X_POSITION, float VIEW_Y_POSITION) {
		super();

		this.VIEW_X_POSITION = VIEW_X_POSITION;
		this.VIEW_Y_POSITION = VIEW_Y_POSITION;

		buildView();

	}

	private void buildView() {
		buttongroup1 = new ButtonGroup<ImageButton>();
		buttongroup2 = new ButtonGroup<ImageButton>();

		firstAmountTable = buildTable(buttongroup1, 1);
		secondAmountTable = buildTable(buttongroup2, 2);

		firstTableHeader = new Label("F�rsta numret", SkinService.getUiSkin(), "medium-font");
		firstTableHeader.setColor(Color.BLACK);
		secondTableHeader = new Label("Andra numret", SkinService.getUiSkin(), "medium-font");
		secondTableHeader.setColor(Color.BLACK);

		tablesContainer = new Table();
		tablesContainer.add(firstTableHeader).padBottom(20f).padRight(200f);
		tablesContainer.add(secondTableHeader).padBottom(20f);
		tablesContainer.row();
		tablesContainer.add(firstAmountTable).padRight(200f);
		tablesContainer.add(secondAmountTable);

		this.add(tablesContainer).center();
	}

	private Table buildTable(ButtonGroup<ImageButton> buttongroup, int tableId) {
		Table table;
		table = new Table();

		table.add(buildButton("one", 1, buttongroup, tableId)).size(200f, 200f).pad(10f);
		table.add(buildButton("two", 2, buttongroup, tableId)).size(200f, 200f).pad(10f);
		table.row();
		table.add(buildButton("three", 3, buttongroup, tableId)).size(200f, 200f).pad(10f);
		table.add(buildButton("four", 4, buttongroup, tableId)).size(200f, 200f).pad(10f);

		buttongroup.setMaxCheckCount(1);
		buttongroup.setMinCheckCount(0);
		buttongroup.uncheckAll();

		this.add(table).center();

		return table;
	}

	private ImageButton buildButton(String name, final int value, ButtonGroup<ImageButton> buttongroup,
			final int tableId) {
		Drawable up = SkinService.getAmountSkin().getDrawable(name + "up");
		Drawable down = SkinService.getAmountSkin().getDrawable(name + "down");
		ImageButton imageButton;
		imageButton = new ImageButton(up, up, down);
		imageButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				addAmount(value, tableId);
				checkAllowEndTurn();
			}

		});

		buttongroup.add(imageButton);

		return imageButton;
	}

	public void addAmount(int value, int tableId) {
		if (tableId == 1) {
			KlunkResolverService.getKlunkDecision().setKlunkAmountFirst(value);
		}

		if (tableId == 2) {
			KlunkResolverService.getKlunkDecision().setKlunkAmountSecond(value);
		}
	}

	public void checkAllowEndTurn() {

		if (buttongroup1.getChecked() != null && buttongroup2.getChecked() != null) {
			TurnService.setAllowEndTurn(true);
		}
		else {
			TurnService.setAllowEndTurn(false);
		}
	}

}
