package se.tablemobilegames.klunkwars.ui.actionviews;

public interface ActionView {

	void setVisible(boolean b);

}
