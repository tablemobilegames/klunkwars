package se.tablemobilegames.klunkwars.modelobjects;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

import se.tablemobilegames.klunkwars.common.AvatarIdEnum;

public class Avatar {

	private final AvatarIdEnum id;
	private final String defaultName;
	private final Drawable defaultDrawable;
	private final Drawable bwDrawable;
	private boolean used;

	public Avatar(AvatarIdEnum id, Drawable defaultDrawable, Drawable bwDrawable) {
		this.id = id;
		this.defaultName = id.getValue();
		this.defaultDrawable = defaultDrawable;
		this.bwDrawable = bwDrawable;
		used = false;
	}

	public AvatarIdEnum getId() {
		return this.id;
	}

	public String getDefaultName() {
		return this.defaultName;
	}

	public Drawable getDefaultDrawable() {
		return defaultDrawable;
	}

	public Drawable getBwDrawable() {
		return bwDrawable;
	}

	public boolean isUsed() {
		return this.used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

}
