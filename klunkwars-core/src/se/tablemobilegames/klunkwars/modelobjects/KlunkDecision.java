package se.tablemobilegames.klunkwars.modelobjects;

import java.util.ArrayList;
import java.util.List;

import se.tablemobilegames.klunkwars.common.ArithmeticEnum;

public class KlunkDecision {

	private int klunkAmountFirst;
	private int klunkAmountSecond;
	private ArithmeticEnum arithmetic;
	private List<Player> targets = new ArrayList<Player>();

	public KlunkDecision() {
		super();
	}

	private int calculateResult() {

		int result = 0;

		if (arithmetic == ArithmeticEnum.PLUS) {
			result = klunkAmountFirst + klunkAmountSecond;
		}

		if (arithmetic == ArithmeticEnum.MINUS) {
			result = klunkAmountFirst - klunkAmountSecond;
		}

		if (arithmetic == ArithmeticEnum.TIMES) {
			result = klunkAmountFirst * klunkAmountSecond;
		}

		return result;

	}

	public void setKlunkAmountFirst(int klunkAmountFirst) {
		this.klunkAmountFirst = klunkAmountFirst;
	}

	public void setKlunkAmountSecond(int klunkAmountSecond) {
		this.klunkAmountSecond = klunkAmountSecond;
	}

	public void setArithmetic(ArithmeticEnum arithmetic) {
		this.arithmetic = arithmetic;
		System.out.println("Arithmetic set");
	}

	public int getKlunkAmountFirst() {
		return klunkAmountFirst;
	}

	public int getKlunkAmountSecond() {
		return klunkAmountSecond;
	}

	public ArithmeticEnum getArithmetic() {
		return arithmetic;
	}

	public List<Player> getTargets() {
		return targets;
	}

	public int getResult() {
		return calculateResult();
	}

	public void addTarget(Player player) {
		targets.add(player);
	}

	public void removeTarget(Player player) {
		targets.remove(player);
	}

}
