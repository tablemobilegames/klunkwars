package se.tablemobilegames.klunkwars.modelobjects;

import java.util.Random;

import se.tablemobilegames.klunkwars.common.KlunkDecisionRoleEnum;

public class Player {

	private String Id;
	private String name;
	private int klunkStones;
	private boolean inPlay;
	private int turnNumber;
	private KlunkDecisionRoleEnum decisionRole;
	private Avatar avatar;

	public Player() {
		super();
		this.Id = generateNewId();
		inPlay = false;
		this.klunkStones = 0;

	}

	private String generateNewId() {
		final Random rand = new Random();
		final String randomId = Integer.toString(rand.nextInt(1000));
		System.out.println("New id created : " + randomId);
		//TODO need to check that id doesnt exist before return
		return randomId;

	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addKlunkStones(int stonesToAdd) {
		this.klunkStones += stonesToAdd;

		//TODO if klunkstones are more than highest limit = lose game

	}

	public void removeKlunkStones(int stonesToRemove) {

		if (this.klunkStones < stonesToRemove) {
			System.out.println("Can't remove that many klunkstones");
		}
		else {
			this.klunkStones -= stonesToRemove;
		}
	}

	public int getKlunkStones() {
		return this.klunkStones;
	}

	public boolean isInPlay() {
		return inPlay;
	}

	public void setInPlay(boolean inPlay) {
		this.inPlay = inPlay;
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	public void setTurnNumber(int turnNumber) {
		this.turnNumber = turnNumber;
	}

	public Avatar getAvatar() {
		return avatar;
	}

	public void setAvatar(Avatar avatar) {
		this.avatar = avatar;
	}

	public KlunkDecisionRoleEnum getDecisionRole() {
		return decisionRole;
	}

	public void setDecisionRole(KlunkDecisionRoleEnum decisionRole) {
		this.decisionRole = decisionRole;
	}

}
