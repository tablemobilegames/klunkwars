package se.tablemobilegames.klunkwars.services;

import se.tablemobilegames.klunkwars.common.PhaseEnum;

public class PhaseService {

	private static PhaseEnum phase = PhaseEnum.PHASE_0;

	public static PhaseEnum getCurrentPhase() {
		return phase;
	}

	public static void switchPhase() {
		switch (phase) {
		case PHASE_0:
			TurnService.initPlayersTurnOrders();
			KlunkResolverService.switchDeciders();
			phase = PhaseEnum.PHASE_1;
			break;
		case PHASE_1:
			phase = PhaseEnum.PHASE_2;
			break;

		case PHASE_2:
			KlunkResolverService.deliverKlunks();
			RulesService.decideIfPlayersStillInGame();
			phase = PhaseEnum.PHASE_3;
			break;

		case PHASE_3:
			TurnService.updatePlayersTurnOrders();
			KlunkResolverService.switchDeciders();
			phase = PhaseEnum.PHASE_1;
			break;

		}
	}

}
