package se.tablemobilegames.klunkwars.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import se.tablemobilegames.klunkwars.common.AvatarIdEnum;
import se.tablemobilegames.klunkwars.modelobjects.Avatar;

public class AvatarService {

	private static List<Avatar> avatars = new ArrayList<Avatar>();
	private static Random rand = new Random();

	private static void buildAvatars() {
		for (final AvatarIdEnum id : AvatarIdEnum.values()) {
			avatars.add(new Avatar(id, SkinService.getAvatarSkin().getDrawable(id.getValue()),
					SkinService.getAvatarSkin().getDrawable(id.getValue() + "bw")));
		}

	}

	public static List<Avatar> getAvatars() {
		if (avatars.isEmpty()) {
			buildAvatars();
		}
		return avatars;
	}

	public static Avatar getAvatar(AvatarIdEnum avatarId) {
		Avatar result = null;
		if (avatars.isEmpty()) {
			buildAvatars();
		}
		for (final Avatar avatar : avatars) {
			if (avatar.getId().equals(avatarId)) {
				result = avatar;
			}
		}
		return result;

	}

	public static Avatar getRandomAvatar() {
		if (avatars.isEmpty()) {
			buildAvatars();
		}
		boolean foundAvailable = false;
		Avatar avatar = null;
		int pick = rand.nextInt(AvatarIdEnum.values().length);
		while (!foundAvailable) {
			if (avatars.get(pick).isUsed() == false) {
				avatar = avatars.get(pick);
				foundAvailable = true;
			}
			else {
				pick = rand.nextInt(AvatarIdEnum.values().length);
			}
		}

		return avatar;
	}

	public static Avatar getRandomAvatarToUse() {
		Avatar avatar = getRandomAvatar();
		avatar.setUsed(true);
		return avatar;
	}

}
