package se.tablemobilegames.klunkwars.services;

import java.util.ArrayList;
import java.util.List;

import se.tablemobilegames.klunkwars.modelobjects.Player;

public class RulesService {

	private static int maxKlunks = 10;
	private static boolean alcoholicMode = true;
	private static List<Player> defeatedPlayersThisRound = new ArrayList<Player>();

	public static int getMaxKlunks() {
		return maxKlunks;
	}

	public static void setMaxKlunks(int maxKlunks) {
		RulesService.maxKlunks = maxKlunks;
	}

	public static boolean isAlcoholicMode() {
		return alcoholicMode;
	}

	public static void setAlcoholicMode(boolean alcoholicMode) {
		RulesService.alcoholicMode = alcoholicMode;
	}

	public static void decideIfPlayersStillInGame() {
		for (Player player : PlayerService.getActivePlayers()) {
			if (player.getKlunkStones() >= maxKlunks) {
				PlayerService.setPlayerNotActive(player);
				System.out.println(player.getName() + " was defeated!");
				defeatedPlayersThisRound.add(player);
			}
		}
	}

	public static String presentDefeatedPlayersThisRound() {

		String result = "";

		if (!defeatedPlayersThisRound.isEmpty()) {

			result = "Besegrade spelare denna runda: ";

			for (Player player : defeatedPlayersThisRound) {
				result += player.getName() + ", ";
			}

			result = result.substring(0, result.length() - 2); //f� bort sista ", "

			//reset list
			defeatedPlayersThisRound = new ArrayList<Player>();
		}

		return result;
	}

}
