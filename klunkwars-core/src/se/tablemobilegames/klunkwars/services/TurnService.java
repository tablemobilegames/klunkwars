package se.tablemobilegames.klunkwars.services;

import java.util.List;

import se.tablemobilegames.klunkwars.common.KlunkDecisionRoleEnum;
import se.tablemobilegames.klunkwars.common.PhaseEnum;
import se.tablemobilegames.klunkwars.modelobjects.Player;

public class TurnService {

	private static int currentTurnNumber = 1;
	private static int lastTurnNumber = PlayerService.getActivePlayers().size();
	private static boolean allowEndTurn = false;

	public static void endPlayerTurn() {

		if (isLastTurn()) {
			currentTurnNumber = findNextTurnNumberInPlay(1);
			PhaseService.switchPhase();
		}
		else {
			currentTurnNumber++;
			currentTurnNumber = findNextTurnNumberInPlay(currentTurnNumber);
		}

		PlayerService.setCurrentPlayerByTurnNumber(currentTurnNumber);
	}

	private static int findNextTurnNumberInPlay(int turnNumber) {
		boolean foundPlayerInPlay = false;
		while (!foundPlayerInPlay) {
			if (PlayerService.isPlayerActiveWithTurnNumber(turnNumber)) {
				foundPlayerInPlay = true;
			}
			else {
				turnNumber++;
				if (turnNumber >= lastTurnNumber) {
					turnNumber = 1;
				}
			}

		}
		return turnNumber;
	}

	public static void initPlayersTurnOrders() {
		int turnNumber;
		for (int i = 0; i < PlayerService.getActivePlayers().size(); i++) {
			turnNumber = i + 1;
			Player player = PlayerService.getActivePlayers().get(i);
			player.setTurnNumber(turnNumber);
			System.out.println("Player " + player.getName() + " got turn number " + turnNumber);
		}
		PlayerService.setCurrentPlayerByTurnNumber(1);

	}

	public static void updatePlayersTurnOrders() {
		int newturnNumber = 0;
		List<Player> playersSortedByTurnNumber = PlayerService.getActivePlayersSortedByTurnNumber();

		for (Player player : playersSortedByTurnNumber) {
			newturnNumber += 1;
			player.setTurnNumber(newturnNumber);
			System.out.println("Player " + player.getName() + " got turn number " + newturnNumber);
		}
		updateLastTurnNumber();
		PlayerService.setCurrentPlayerByTurnNumber(1);
	}

	public static boolean isLastTurn() {
		boolean result = false;
		if (currentTurnNumber >= lastTurnNumber) {
			result = true;
		}
		return result;

	}

	private static void updateLastTurnNumber() {
		lastTurnNumber = PlayerService.getActivePlayers().size();
	}

	public static int getCurrentTurnNumber() {
		return currentTurnNumber;
	}

	public static int getLastTurnNumber() {
		return lastTurnNumber;
	}

	public static void setAllowEndTurn(boolean value) {
		allowEndTurn = value;
	}

	public static boolean getAllowEndTurn() {
		checkExitTurncondition();

		return allowEndTurn;
	}

	private static void checkExitTurncondition() {

		PhaseEnum currentPhase = PhaseService.getCurrentPhase();
		KlunkDecisionRoleEnum decisionRole = PlayerService.getCurrentPlayer().getDecisionRole();

		if (currentPhase == PhaseEnum.PHASE_1 && decisionRole == KlunkDecisionRoleEnum.NoDecider) {
			setAllowEndTurn(true);
		}

		if (currentPhase == PhaseEnum.PHASE_2) {
			//TODO only allow if player have picked role
			setAllowEndTurn(true);
		}

	}

}
