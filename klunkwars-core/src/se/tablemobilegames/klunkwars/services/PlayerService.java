package se.tablemobilegames.klunkwars.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.tablemobilegames.klunkwars.common.TurnNumberComparator;
import se.tablemobilegames.klunkwars.modelobjects.Player;

public class PlayerService {

	private static List<Player> players = new ArrayList<Player>();
	private static Player currentPlayer = null;

	public static List<Player> getAllPlayers() {
		return players;
	}

	public static List<Player> getActivePlayers() {
		List<Player> playersInPlay = new ArrayList<Player>();
		for (Player player : players) {
			if (player.isInPlay()) {
				playersInPlay.add(player);
			}
		}
		return playersInPlay;
	}

	public static void addPlayer(Player player) {
		players.add(player);
		System.out.println("Added player " + player.getId());
	}

	public static void removePlayer(Player player) {
		players.remove(player);
		System.out.println("Removed player " + player.getId());
	}

	public static void setPlayerNotActive(Player playerNotToPlay) {
		for (Player player : players) {
			if (player.equals(playerNotToPlay)) {
				player.setInPlay(false);
				System.out.println("Changed Player " + player.getId() + " in play to " + player.isInPlay());
			}
		}
	}

	public static Player getCurrentPlayer() {
		return currentPlayer;
	}

	public static void setCurrentPlayerByTurnNumber(int turnNumber) {

		currentPlayer = getPlayerByTurnNumber(turnNumber);
	}

	public static Player getPlayerById(String id) {
		Player playerById = null;

		for (final Player player : players) {
			if (player.getId().equals(id)) {
				playerById = player;
			}
		}
		return playerById;
	}

	public static Player getPlayerByTurnNumber(int turnNumber) {
		Player playerByTurnNumber = null;

		for (final Player player : players) {
			if (player.getTurnNumber() == turnNumber) {
				playerByTurnNumber = player;
			}
		}
		return playerByTurnNumber;
	}

	public static void resetPlayerList() {
		players = new ArrayList<Player>();
	}

	public static boolean isPlayerActiveWithTurnNumber(int turnNumber) {
		if (turnNumber > TurnService.getLastTurnNumber()) {
			return false;
		}

		return getPlayerByTurnNumber(turnNumber).isInPlay();
	}

	public static Player findNextPlayerInTurn(int turn) {
		Player result = null;
		int searchTurn = turn + 1;

		boolean foundPlayerInPlay = false;
		while (!foundPlayerInPlay) {
			if (isPlayerActiveWithTurnNumber(searchTurn)) {
				result = getPlayerByTurnNumber(searchTurn);
				foundPlayerInPlay = true;
			}
			else {
				searchTurn++;
				if (searchTurn > TurnService.getLastTurnNumber()) {
					searchTurn = 1;
				}
			}
		}
		return result;
	}

	public static List<Player> getActivePlayersSortedByTurnNumber() {
		List<Player> result = getActivePlayers();
		Collections.sort(result, new TurnNumberComparator());
		return result;
	}

}
