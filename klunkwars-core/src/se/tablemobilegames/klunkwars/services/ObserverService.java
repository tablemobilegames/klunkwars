package se.tablemobilegames.klunkwars.services;

import java.util.ArrayList;
import java.util.List;

import se.tablemobilegames.klunkwars.common.KlunkObserver;

public class ObserverService {

	private static List<KlunkObserver> klunkObservers = new ArrayList<KlunkObserver>();

	public static void updateKlunks() {
		notifyAllKlunkObservers();
	}

	public static void attach(KlunkObserver observer) {
		klunkObservers.add(observer);
	}

	public static void notifyAllKlunkObservers() {
		for (KlunkObserver observer : klunkObservers) {
			observer.update();
		}
	}
}
