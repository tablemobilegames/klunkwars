package se.tablemobilegames.klunkwars.services;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import se.tablemobilegames.klunkwars.application.KlunkwarsGame;
import se.tablemobilegames.platform.utils.SmartFontGenerator;

public class SkinService {

	private static AssetManager assets;
	private static Skin uiSkin;
	private static Skin avatarSkin;
	private static Skin amountSkin;
	private static Skin arithmeticSkin;
	private static SmartFontGenerator fontGen = new SmartFontGenerator();
	private final static FileHandle exoFile = Gdx.files.internal("liberationmonoregular.ttf");

	public static void initSkins(KlunkwarsGame game) {
		assets = game.getAssetManager();
		fontGen = new SmartFontGenerator();
		uiSkin = new Skin();
		avatarSkin = new Skin();
		amountSkin = new Skin();
		arithmeticSkin = new Skin();

		uiSkin.addRegions(assets.get("ui/uiskin.atlas", TextureAtlas.class));
		uiSkin.add("default-font", fontGen.createFont(exoFile, "exo-medium", 48));
		uiSkin.add("tiny-font", fontGen.createFont(exoFile, "exo-small", 20));
		uiSkin.add("small-font", fontGen.createFont(exoFile, "exo-small", 28));
		uiSkin.add("medium-font", fontGen.createFont(exoFile, "exo-medium", 48));
		uiSkin.add("large-font", fontGen.createFont(exoFile, "exo-medium", 80));
		uiSkin.load(Gdx.files.internal("ui/uiskin.json"));

		avatarSkin.addRegions(assets.get("avatars/avatarskin.atlas", TextureAtlas.class));
		amountSkin.addRegions(assets.get("amounts/amount.atlas", TextureAtlas.class));
		arithmeticSkin.addRegions(assets.get("arithmetics/arithmetics.atlas", TextureAtlas.class));
	}

	public static Skin getUiSkin() {
		return uiSkin;
	}

	public static Skin getAvatarSkin() {
		return avatarSkin;
	}

	public static Skin getAmountSkin() {
		return amountSkin;
	}

	public static Skin getArithmeticSkin() {
		return arithmeticSkin;
	}

}
