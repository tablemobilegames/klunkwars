package se.tablemobilegames.klunkwars.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import se.tablemobilegames.klunkwars.common.ArithmeticEnum;
import se.tablemobilegames.klunkwars.common.KlunkDecisionRoleEnum;
import se.tablemobilegames.klunkwars.modelobjects.KlunkDecision;
import se.tablemobilegames.klunkwars.modelobjects.Player;

public class KlunkResolverService {

	private static KlunkDecision klunkDecision;
	private static Player pickAmountPlayer;
	private static Player pickArithmeticPlayer;
	private static Player pickTargetsPlayer;

	private static int amountOfKlunks;
	private static List<Player> playersToGetKlunks;

	private static int numOfTargetsToPick;

	public static KlunkDecision getKlunkDecision() {
		return klunkDecision;
	}

	public static Player getPickAmountPlayer() {
		return pickAmountPlayer;
	}

	public static Player getPickArithmeticPlayer() {
		return pickArithmeticPlayer;
	}

	public static Player getPickTargetsPlayer() {
		return pickTargetsPlayer;
	}

	public static void switchDeciders() {
		klunkDecision = new KlunkDecision();

		if (PlayerService.getActivePlayers().size() >= 3) {
			switchWithThreeOrMorePlayers();
		}

		if (PlayerService.getActivePlayers().size() == 2) {
			switchWithTwoPlayers();
		}

	}

	private static void switchWithTwoPlayers() {
		numOfTargetsToPick = 1;

		if (pickAmountPlayer == null) {
			pickAmountPlayer = PlayerService.findNextPlayerInTurn(0);
		}
		else {
			pickAmountPlayer = PlayerService.findNextPlayerInTurn(pickAmountPlayer.getTurnNumber());
		}

		pickArithmeticPlayer = null;

		pickTargetsPlayer = PlayerService.findNextPlayerInTurn(pickAmountPlayer.getTurnNumber());
		autoResolveArithmeticDecision();

		//change value in players
		pickAmountPlayer.setDecisionRole(KlunkDecisionRoleEnum.Amount);
		pickTargetsPlayer.setDecisionRole(KlunkDecisionRoleEnum.Targets);
	}

	private static void switchWithThreeOrMorePlayers() {
		numOfTargetsToPick = 2;

		if (pickAmountPlayer == null) {
			pickAmountPlayer = PlayerService.findNextPlayerInTurn(0);
		}
		else {
			pickAmountPlayer = PlayerService.findNextPlayerInTurn(pickAmountPlayer.getTurnNumber());
		}

		pickArithmeticPlayer = PlayerService.findNextPlayerInTurn(pickAmountPlayer.getTurnNumber());
		pickTargetsPlayer = PlayerService.findNextPlayerInTurn(pickArithmeticPlayer.getTurnNumber());

		//change value in players
		pickAmountPlayer.setDecisionRole(KlunkDecisionRoleEnum.Amount);
		pickArithmeticPlayer.setDecisionRole(KlunkDecisionRoleEnum.Arithmetic);
		pickTargetsPlayer.setDecisionRole(KlunkDecisionRoleEnum.Targets);

		for (Player player : PlayerService.getActivePlayers()) {

			if ((player != pickAmountPlayer) && (player != pickArithmeticPlayer) && (player != pickTargetsPlayer)) {
				player.setDecisionRole(KlunkDecisionRoleEnum.NoDecider);
			}
		}

	}

	private static void autoResolveArithmeticDecision() {
		Random rand = new Random();
		if (rand.nextInt() % 2 == 0) {
			klunkDecision.setArithmetic(ArithmeticEnum.PLUS);
		}
		else {
			klunkDecision.setArithmetic(ArithmeticEnum.MINUS);
		}
	}

	public static void setPlayersToGetKlunks() {
		int result = klunkDecision.getResult();
		playersToGetKlunks = new ArrayList<Player>();

		for (Player player : PlayerService.getActivePlayers()) {
			if (result > 0) {
				if (klunkDecision.getTargets().contains(player)) {
					playersToGetKlunks.add(player);
				}
			}
			else {
				if (!klunkDecision.getTargets().contains(player)) {
					playersToGetKlunks.add(player);
				}
			}

		}

	}

	private static void setAmountOfKlunks() {
		int result = klunkDecision.getResult();

		if (result < 0) {
			amountOfKlunks = result *= -1;
		}
		else {
			amountOfKlunks = result;
		}

	}

	public static void deliverKlunks() {
		setAmountOfKlunks();
		setPlayersToGetKlunks();

		for (Player player : playersToGetKlunks) {
			player.addKlunkStones(amountOfKlunks);
		}

	}

	public static String presentResult() {

		String result = amountOfKlunks + " st klunkar till: ";

		for (Player player : playersToGetKlunks) {
			result += player.getName() + ", ";
		}

		result = result.substring(0, result.length() - 2); //f� bort sista ", "

		return result;
	}

	public static int getNumOfTargetsToPick() {
		return numOfTargetsToPick;
	}

}
