package se.tablemobilegames.klunkwars.common;

public enum AvatarIdEnum {
	ALDO("Aldo"), AMADEUS("Amadeus"), BORIS("Boris"), DMITRIJ("Dmitrij"), DONNY("Donny"), FJODOR("Fjodor"), IGOR(
			"Igor"), IVAN("Ivan"), LUDMILA("Ludmila"), MAURITZ("Mauritz"), NINA("Nina"), OLEG("Oleg"), OMAR(
					"Omar"), PIETR("Pietr"), ROMAN("Roman"), RUDOLF("Rudolf"), SASJA(
							"Sasja"), SERGEJ("Sergej"), TANJA("Tanja"), VLADLENA("Vladlena"), YURI("Yuri");

	private final String value;

	AvatarIdEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
