package se.tablemobilegames.klunkwars.common;

public enum ArithmeticEnum {
	PLUS("+"), MINUS("-"), TIMES("*");

	private String value;

	ArithmeticEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
