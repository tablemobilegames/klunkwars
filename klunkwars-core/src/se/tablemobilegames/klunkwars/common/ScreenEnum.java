package se.tablemobilegames.klunkwars.common;

public enum ScreenEnum {
	LOADING_SCREEN, MAIN_MENU, SETUP_NEW_GAME, TURN_LOBBY, PRESENT_CARDS, PLAY, RESULT;
}
