package se.tablemobilegames.klunkwars.common;

public interface KlunkObserver {

	public void update();

	public void attachKlunkObserver();
}
