package se.tablemobilegames.klunkwars.common;

public enum KlunkDecisionRoleEnum {
	Amount("AMOUNT"), Arithmetic("ARITHMETIC"), Targets("TARGETS"), NoDecider("");

	private final String value;

	KlunkDecisionRoleEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
