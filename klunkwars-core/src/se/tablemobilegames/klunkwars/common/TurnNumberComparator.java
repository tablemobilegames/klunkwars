package se.tablemobilegames.klunkwars.common;

import java.util.Comparator;

import se.tablemobilegames.klunkwars.modelobjects.Player;

public class TurnNumberComparator implements Comparator<Player> {

	@Override
	public int compare(Player p1, Player p2) {
		return p1.getTurnNumber() - p2.getTurnNumber();
	}

}
