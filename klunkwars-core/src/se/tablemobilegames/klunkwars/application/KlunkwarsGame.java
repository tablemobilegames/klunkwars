package se.tablemobilegames.klunkwars.application;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.platform.gamestate.AccessGamestate;
import se.tablemobilegames.platform.gamestate.Gamestate;
import se.tablemobilegames.platform.interfaces.TableMobileGame;

public class KlunkwarsGame extends Game implements TableMobileGame {

	private OrthographicCamera camera;
	private SpriteBatch batch;

	private ScreenNavigator screenHandler;
	private GamestateImpl gamestate;
	private AssetManager assets;
	private static int WIDTH;
	private static int HEIGHT;

	@Override
	public void create() {

		WIDTH = Gdx.app.getGraphics().getWidth();
		HEIGHT = Gdx.app.getGraphics().getHeight();
		assets = new AssetManager();

		this.camera = new OrthographicCamera();
		this.camera.setToOrtho(false, WIDTH, HEIGHT);
		this.batch = new SpriteBatch();

		this.gamestate = (GamestateImpl) AccessGamestate.getGameState(this);
		this.gamestate.setTest("jag �r den nya stringen");
		AccessGamestate.saveGamestate("2017-06-16");
		this.gamestate = (GamestateImpl) AccessGamestate.loadGamestate(new GamestateImpl(), "2017-06-15");
		System.out.println(this.gamestate.getTest());

		screenHandler = new ScreenNavigator(this);
		screenHandler.switchScreen(ScreenEnum.LOADING_SCREEN);

	}

	@Override
	public void render() {
		super.render();

	}

	@Override
	public void dispose() {
		this.batch.dispose();
		this.assets.dispose();
		this.screenHandler.disposeAllScreens();

	}

	@Override
	public SpriteBatch getBatch() {
		return this.batch;
	}

	@Override
	public Gamestate createGameState() {
		return new GamestateImpl();
	}

	@Override
	public float getWidth() {
		return WIDTH;
	}

	@Override
	public float getHeight() {
		return HEIGHT;
	}

	@Override
	public OrthographicCamera getCamera() {
		return this.camera;
	}

	@Override
	public AssetManager getAssetManager() {
		return this.assets;
	}

	@Override
	public void changeScreenAtNewGame() {
		screenHandler.switchScreen(ScreenEnum.SETUP_NEW_GAME);
	}

	public void changeScreen(ScreenEnum screen) {
		screenHandler.switchScreen(screen);
	}

}
