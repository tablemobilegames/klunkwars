package se.tablemobilegames.klunkwars.application;

import se.tablemobilegames.platform.gamestate.Gamestate;

public class GamestateImpl implements Gamestate {

	private String test;

	public String getTest() {
		return this.test;
	}

	public void setTest(String test) {
		this.test = test;
	}

}