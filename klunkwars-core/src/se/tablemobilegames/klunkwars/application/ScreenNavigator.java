package se.tablemobilegames.klunkwars.application;

import se.tablemobilegames.klunkwars.common.ScreenEnum;
import se.tablemobilegames.klunkwars.ui.screens.LoadingScreen;
import se.tablemobilegames.klunkwars.ui.screens.MainMenuScreen;
import se.tablemobilegames.klunkwars.ui.screens.PlayScreen;
import se.tablemobilegames.klunkwars.ui.screens.PresentCardsScreen;
import se.tablemobilegames.klunkwars.ui.screens.ResultScreen;
import se.tablemobilegames.klunkwars.ui.screens.SetupNewGameScreen;
import se.tablemobilegames.klunkwars.ui.screens.TurnLobbyScreen;

public class ScreenNavigator {

	private final KlunkwarsGame game;
	private final LoadingScreen loadingScreen;
	private final MainMenuScreen mainMenuScreen;
	private final SetupNewGameScreen setupNewGameScreen;
	private final TurnLobbyScreen turnLobby;
	private final PresentCardsScreen presentCardsScreen;
	private final PlayScreen playScreen;
	private final ResultScreen resultScreen;

	public ScreenNavigator(KlunkwarsGame game) {
		super();
		this.game = game;
		loadingScreen = new LoadingScreen(this.game);
		mainMenuScreen = new MainMenuScreen(this.game);
		setupNewGameScreen = new SetupNewGameScreen(this.game);
		turnLobby = new TurnLobbyScreen(this.game);
		presentCardsScreen = new PresentCardsScreen(this.game);
		playScreen = new PlayScreen(this.game);
		resultScreen = new ResultScreen(this.game);

	}

	public void switchScreen(ScreenEnum screen) {

		switch (screen) {
		case LOADING_SCREEN:
			game.setScreen(loadingScreen);
			break;

		case MAIN_MENU:
			game.setScreen(mainMenuScreen);
			break;

		case SETUP_NEW_GAME:
			game.setScreen(setupNewGameScreen);
			break;

		case TURN_LOBBY:
			game.setScreen(turnLobby);
			break;

		case PRESENT_CARDS:
			game.setScreen(presentCardsScreen);
			break;

		case PLAY:
			game.setScreen(playScreen);
			break;

		case RESULT:
			game.setScreen(resultScreen);
			break;
		}
	}

	public void disposeAllScreens() {
		loadingScreen.dispose();
		mainMenuScreen.dispose();
		setupNewGameScreen.dispose();
		turnLobby.dispose();
		presentCardsScreen.dispose();
		playScreen.dispose();
	}

}
