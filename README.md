# README #

Klunkwars is a table-mobile game where players compete in a similar manner to 'last man standing'. 
If you get too many klunkstones you lose.

The idea of a table-mobile game is to meet and interact face to face with your friends as if you where playing a board game,
while at the same time get access to the technology, mobility and easy setup of a mobile app.

### WIKI ###
Check out the wiki for:	

* Rules of Klunkwars the boardgame Swedish	


### How do I get set up? ###

* [Setup your development environment](https://github.com/libgdx/libgdx/wiki)	
* Clone the repository or download and extract the ZIP file	
* Import the project into your preferred development environment, run, debug and package it!	
	+ [Eclipse](https://github.com/libgdx/libgdx/wiki/Gradle-and-Eclipse)	
	+ [Intellij IDEA](https://github.com/libgdx/libgdx/wiki/Gradle-and-Intellij-IDEA)	
	+ [NetBeans](https://github.com/libgdx/libgdx/wiki/Gradle-and-NetBeans)		
	+ [Gradle on the Commandline](https://github.com/libgdx/libgdx/wiki/Gradle-on-the-Commandline)			
	