package se.tablemobilegames.platform.interfaces;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import se.tablemobilegames.platform.gamestate.Gamestate;

public interface TableMobileGame {

	public SpriteBatch getBatch();

	public Gamestate createGameState();

	public float getWidth();

	public float getHeight();

	public OrthographicCamera getCamera();

	public AssetManager getAssetManager();

	public void changeScreenAtNewGame();

}
