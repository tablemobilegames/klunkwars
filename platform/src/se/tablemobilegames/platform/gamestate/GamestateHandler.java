package se.tablemobilegames.platform.gamestate;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

import se.tablemobilegames.platform.interfaces.TableMobileGame;

public class GamestateHandler {

	private Gamestate currentGameState;

	protected Gamestate getGameState(TableMobileGame tableMobileGame) {
		if (this.currentGameState == null) {
			this.currentGameState = tableMobileGame.createGameState();
		}

		return this.currentGameState;

	}

	protected void saveGamestate(String saveName) {
		FileHandle saveFile = Gdx.files.local("saves/" + saveName + ".json");
		if (!saveFile.exists()) {
			Json json = new Json();
			saveFile.writeString(json.prettyPrint(this.currentGameState), false);
		}
	}

	protected Gamestate loadGameState(Gamestate gamestate, String gameStateName) {
		FileHandle loadFile = Gdx.files.local("saves/" + gameStateName + ".json");
		if (loadFile.exists()) {
			Json json = new Json();
			gamestate = json.fromJson(gamestate.getClass(), loadFile.readString());
		}

		return gamestate;
	}

	protected void resetGameState(TableMobileGame tableMobileGame) {
		this.currentGameState = tableMobileGame.createGameState();
	}

}
