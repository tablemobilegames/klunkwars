package se.tablemobilegames.platform.gamestate;

import se.tablemobilegames.platform.interfaces.TableMobileGame;

public class AccessGamestate {

	private GamestateHandler gamestateHandler = new GamestateHandler();

	public enum Access {

		gamestateHandler(new GamestateHandler());

		GamestateHandler value;

		Access(GamestateHandler gamestateHandler) {
			this.value = gamestateHandler;
		}
	}

	public static Gamestate getGameState(TableMobileGame tableMobileGame) {
		return Access.gamestateHandler.value.getGameState(tableMobileGame);

	}

	public static void saveGamestate(String saveName) {
		Access.gamestateHandler.value.saveGamestate(saveName);
	}

	public static Gamestate loadGamestate(Gamestate gamestate, String gameStateName) {
		return Access.gamestateHandler.value.loadGameState(gamestate, gameStateName);

	}

	public static void resetGamestate(TableMobileGame tableMobileGame) {
		Access.gamestateHandler.value.resetGameState(tableMobileGame);
	}

	public GamestateHandler getGamestateHandler() {
		return this.gamestateHandler;
	}

	public void setGamestateHandler(GamestateHandler gamestateHandler) {
		this.gamestateHandler = gamestateHandler;
	}

}

